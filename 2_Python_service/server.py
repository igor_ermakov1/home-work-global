from http.server import BaseHTTPRequestHandler, HTTPServer
 
class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/plain")
        self.end_headers()
        self.wfile.write(bytes("Hi!", "utf-8"))  
        #self.wfile.write(bytes(' \nYou IP address: {}\nIm server IP: {}\n'.format(self.client_address, self.address_string()), "utf-8"))
        self.wfile.write(bytes(' \nYou IP address: {}\n'.format(self.address_string()), "utf-8"))
        self.wfile.write(bytes(format(self.requestline), "utf-8"))

        return 
 
if __name__ == '__main__':   
    httpd = HTTPServer(('', 80), Handler)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:    # исключение, вызывается при попытке остановить программу с помощью сочетания клавиш Ctrl + C
        pass
    httpd.server_close()