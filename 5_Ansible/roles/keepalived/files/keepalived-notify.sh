#!/bin/sh
# скрипт будет вызываться демоном keepalived при изменении состояния кластера. Он запишет в каталог /var/run файл для диагностики
umask -S u=rwx,g=rx,o=rx
exec echo "[$(date -Iseconds)]" "$0" "$@" >>"/var/run/keepalived.$1.$2.state"
